
var DropboxAlgoliaBot = require('./Bot/DropboxAlgoliaBot');
var DbxBot = new DropboxAlgoliaBot(true);
var AWS = require('aws-sdk');
var lambda = new AWS.Lambda({region: process.env.AWSLAMBDA_REGION, apiVersion: '2015-03-31'});

exports.handler = async (event, context, callback) => {
    context.callbackWaitsForEmptyEventLoop = false;

    var invokeSubtask = function(subtaskName, data, cb) {
        var params = {
            FunctionName: context.functionName,
            InvocationType: 'RequestResponse',
            Payload: JSON.stringify({
                "subtask" : subtaskName,
                "subtaskData" : data
            })
        };

        lambda.invoke(params, function(err, data) {
            if (err) {
                throw new Error(err);
            } else {
                cb();
            }
        });
    };

    var runDbxFileListParser = function() {
        var cbInQueue = {};

        DbxBot.runNewFile = function(newFileData, newFileCb) {
            cbInQueue[newFileData.id] = newFileCb;
            invokeSubtask('parse-file', newFileData, function() {
                if( 'undefined' != typeof cbInQueue[newFileData.id]) {
                    cbInQueue[newFileData.id]();
                    delete cbInQueue[newFileData.id];
                }
            });
        };

        return new Promise(function(resolve, reject) {
            var checkFinish = function() {
                if (!Object.keys(cbInQueue).length) {
                    return resolve();
                }
                setTimeout(checkFinish, 100);
            };

            DbxBot.on('files-checked-and-cleared', function() {
                checkFinish();
            });
            DbxBot.emit('dropbox-changed');
        });
    };

    var runFileParser = function(data){
        return new Promise(function(resolve, reject) {
            DbxBot.addToQueue(data, function() {
                resolve();
            });
        });
    };

    if (event.queryStringParameters && event.queryStringParameters.challenge) {
        return callback(null, {
            statusCode: 200,
            body: event.queryStringParameters.challenge
        });
    }

    if(event.subtask) {
        switch (event.subtask) {
            case 'parse-file':
                await runFileParser(event.subtaskData);
                break;
        }
    } else {
        await runDbxFileListParser();
    }

    return callback(null, {
        statusCode: 200,
        body: "success"
    });
};


