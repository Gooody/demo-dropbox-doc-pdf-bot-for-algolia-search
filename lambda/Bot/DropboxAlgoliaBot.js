/**
 * Class: DropboxAlgoliaBot
 * Extends: Abstract
 *
 * @method runNewFile
 * @method addToQueue
 */

var Abstract = require('./Abstract');
DropboxAlgoliaBot.prototype = new Abstract();

function DropboxAlgoliaBot(isLamdba)
{
    var self = this;
    this.isLamdba = isLamdba || false;

    // services modules declaration
    var CacheService = 'algolia' === this.getConfig().cache.type.toLowerCase()
            ? require('./Services/Cache/Algolia')
            : require('./Services/Cache/File');
    var AlgoliaService = require('./Services/Algolia');
    var HttpService = require('./Services/Http');
    var QueueService = require('./Services/Queue');

    // initiation services
    var services = {
        http: new HttpService(),
        queue: new QueueService(),
        cache: new CacheService(),
        algolia: new AlgoliaService()
    };

    // initiation parsers
    var parsers = {
        doc:require('./Parsers/Doc'),
        docx:require('./Parsers/Docx'),
        pdf:require('./Parsers/Pdf')
    };

    // declaration dropbox API processors
    var dropboxes = {
        SharingGetFileMetadata: require('./Dropbox/SharingGetFileMetadata'),
        DropboxFilesListFolder: require('./Dropbox/FilesListFolder'),
        FilesDownload: require('./Dropbox/FilesDownload')
    };

    // initiation dropbox processors
    var DropboxFilesListFolder = new dropboxes.DropboxFilesListFolder();
    services.queue.setField('dropboxes', dropboxes).setField('parsers', parsers).setField('services', services);

    /**
     * Event handler for check information about file
     */
    this.on('file', function(fileData, cb){
        services.cache.check(fileData.id, fileData.content_hash, function(cacheRes){
            if (!cacheRes) {
                self.runNewFile(fileData, cb);
            } else {
                cb();
            }
        });
    });

    /**
     * Event handler for file content processing
     */
    this.on('dropbox-changed', function(){
        DropboxFilesListFolder.prepareFiles();
    });

    /**
     * Event handler for remove file information
     */
    this.on('dropbox-removed-file', function(dbxId, cb){
        services.algolia.rm(dbxId, function(){
            console.log('Removed from Algolia', dbxId);
            cb();
        });
    });

    /**
     * Event handler for running preprocessing actions
     */
    this.on('start-check-file-list', function(){
        services.cache.startWatching();
    });

    /**
     * Event handler for running postprocessing actions
     */
    this.on('finish-check-file-list', function(){
        services.cache.endWatching();
    });

    /**
     * Add file to queue for processing
     */
    this.runNewFile = function(fileData, cb) {
        services.queue.add(fileData);
        cb();
    };

    /**
     * Add and processing file
     */
    this.addToQueue = function(fileData, cb) {
        services.queue.add(fileData, cb).run();
    };

    if (!self.isLamdba) {
        DropboxFilesListFolder.prepareFiles();
        services.queue.run();
    }
}

module.exports = DropboxAlgoliaBot;

