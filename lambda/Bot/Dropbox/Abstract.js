/**
 * Class: DropboxAbstract
 *
 * @method callAPI
 */
var Abstract = require('../Abstract');
DropboxAbstract.prototype = new Abstract();

var _connection = false;

function DropboxAbstract() { }

/**
 * universal caller for API
 * @param object query
 * @param function cb
 * @return _connection
 */
DropboxAbstract.prototype.callAPI = function(query, cb){
    var self = this;
    if (!_connection) {
        var dropboxV2Api = require('dropbox-v2-api');
        _connection = dropboxV2Api.authenticate({
            token: self.getConfig().dropbox.token
        });
    }
    return _connection(query, (err, result, response) => {
        if (err) {
            console.error('Dropbox query Error', err);
        }
        return cb(result, response)
    })
};


module.exports = DropboxAbstract;

