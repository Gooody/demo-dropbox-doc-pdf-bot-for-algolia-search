
/**
 * Class: DropboxFilesListFolder
 * Extends: DropboxAbstract
 *
 * @method initFileStream
 */

var DropboxAbstract = require('./Abstract');
DropboxFilesListFolder.prototype = new DropboxAbstract();

function DropboxFilesListFolder(pathData)
{
    var self = this;

    /**
     * processing files/download API resource
     * @param function cbStream
     */
    this.initFileStream = function(cbStream) {
        var parameters = {};
        self.callAPI({
            resource: 'files/download',
            parameters: {
                path: pathData.id || pathData.path || pathData.rev
            }
        }, (result, response) => {
        }).pipe(cbStream);
    }
}

module.exports = DropboxFilesListFolder;

