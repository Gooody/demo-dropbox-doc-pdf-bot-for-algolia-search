
/**
 * Class: SharingGetFileMetadata
 * Extends: DropboxAbstract
 *
 * @method prepareUrl
 */

var DropboxAbstract = require('./Abstract');
SharingGetFileMetadata.prototype = new DropboxAbstract();

function SharingGetFileMetadata(pathData)
{
    var self = this;

    /**
     * processing sharing/get_file_metadata API resource
     */
    this.prepareUrl = function() {
        return new Promise(function(resolve, reject) {
            self.callAPI({
                resource: 'sharing/get_file_metadata',
                parameters: {
                    file: pathData.id || pathData.path || pathData.rev
                }
            }, (result, response) => {
                pathData.url = result.preview_url;
                resolve(pathData);
            });
        });
    }
}

module.exports = SharingGetFileMetadata;

