
/**
 * Class: DropboxFilesListFolder
 * Extends: DropboxAbstract
 *
 * @method prepareFiles
 */

var DropboxAbstract = require('./Abstract');
DropboxFilesListFolder.prototype = new DropboxAbstract();
var async = require('async');

function DropboxFilesListFolder()
{
    var self = this;
    var _isWorking = false;
    var _isDeferredWork = false;

    /**
     * processing files/list_folder API resource
     */
    var prepareFiles = function(cursor) {
        _isWorking = true;
        var params = {};
        if(cursor) {
            params ={
                resource: 'files/list_folder/continue',
                parameters: {
                    'cursor': cursor
                }
            };
        } else {
            params = {
                resource: 'files/list_folder',
                parameters: {
                    'path': self.getConfig().dropbox.path,
                    "recursive": true,
                    "include_mounted_folders": true
                }
            };
        }

        self.callAPI(params, function(result, response) {
            var entries = result.entries || [];
            async.eachOfLimit(entries, self.getConfig().dropbox.queueProcessorNumberOfAtOnce, function(entry, iteration, asyncCb) {
                if ('file' == entry['.tag']){
                    self.emit('file', {
                        id:entry.id,
                        path:entry.path_display,
                        content_hash:entry.content_hash
                    }, function(){
                        asyncCb();
                    });
                } else {
                    asyncCb();
                }
            }, function(){

                if ('undefined' != typeof result.has_more && 'undefined' != typeof result.cursor && result.has_more) {
                    prepareFiles(result.cursor);
                } else {
                    _isWorking = false;
                    if (_isDeferredWork) {
                        _isDeferredWork = false;
                        prepareFiles();
                    } else {
                        self.emit('finish-check-file-list');
                    }
                }
            });
        });
    };

    /**
     * processing files/list_folder API resource
     */
    this.prepareFiles = function() {
        if (!_isWorking) {
            self.emit('start-check-file-list');
            prepareFiles();
        } else {
            _isDeferredWork = true;
        }
    };
}

module.exports = DropboxFilesListFolder;

