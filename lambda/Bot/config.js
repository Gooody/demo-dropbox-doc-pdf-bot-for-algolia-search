
module.exports = {
    http: {
        enabled:false,                                        // true / false
        port:8080                                             //port, proxied from nginx
    },
    algolia: {
        appId:    process.env.ALGOLIA_APPID || '',            //Algolia AppId
        apiKey:   process.env.ALGOLIA_APIKEY || '',           //Algolia admin API Key
        index:    process.env.ALGOLIA_INDEX || '',            //Algolia index name
        isFreeAccounts: true
    },
    dropbox: {
        queueProcessorNumberOfAtOnce: parseInt(process.env.DROPBOX_QUEUE_PROCESSOR_NUMBER_OF_AT_ONCE) || 1,    //number of running file-parsers at the same time
        token:      process.env.DROPBOX_TOKEN || '',          //dropbox app token
        path:       process.env.DROPBOX_PATH || '/test',      //dropbox folder path (if use a root dir - set empty string)
        tmpFolder:  process.env.TMP_FOLDER || '/tmp/'         //  './tmp/files/'
    },
    cache:{
        type: process.env.CACHE_TYPE || 'algolia'             // algolia OR file
    }
};
