/**
 * Class: ParserPdf
 * Extends: ParserAbstract
 *
 * @method onStreamFinish
 */

var PFParser = require("pdf2json/pdfparser");
var ParserAbstract = require('./Abstract');
ParserPdf.prototype = new ParserAbstract();


function ParserPdf(_cb)
{
    var self = this;
    this.setCb(_cb);

    /**
     * Processing data from Stream
     */
    this.onStreamFinish = () => {
        var pdfParser = new PFParser();
        
        pdfParser.on("pdfParser_dataError", function(msg){
            console.error('PDF Parser Data Error', msg);
            self.onReay('');
        });
        pdfParser.on("pdfParser_dataReady", function (pdf) {
            //var bodyArr = [];
            var body = '';
            pdf.data = pdf.data || pdf.formImage;

            if (pdf.data && pdf.data.Pages) {
                for (var p in pdf.data.Pages) {
                    var page = pdf.data.Pages[p];
                    for (var t in page.Texts) {
                        var item = page.Texts[t];
                        //bodyArr.push( decodeURIComponent(item.R[0].T) );
                        body += decodeURIComponent(item.R[0].T) + ' ';
                    }
                }
            }
            //self.onReay(bodyArr.join(' '));
            self.onReay(body);
        });
        pdfParser.loadPDF(self.tmpFilePath, {});
    };
}

module.exports = ParserPdf;

