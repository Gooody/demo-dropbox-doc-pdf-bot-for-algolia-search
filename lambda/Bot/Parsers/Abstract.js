
/**
 * Class: ParserAbstract
 *
 * @property string tmpFilePath
 * @property stream stream
 *
 * @method getStream
 * @method getStream
 * @method onStreamFinish
 * @method setCb
 * @method cb
 * @method onReay
 */
var Abstract = require('../Abstract');
var fs = require('fs');

ParserAbstract.prototype = new Abstract();

function ParserAbstract() { }

/**
 * temporary path to the file
 * @var string
 */
ParserAbstract.prototype.tmpFilePath = '';

/**
 * stream
 * @var bool/stream
 */
ParserAbstract.prototype.stream = false;

/**
 * getter for stream
 * @return stream
 */
ParserAbstract.prototype.getStream = function(path) {
    var self = this;
    console.log('Downloading', path)
    this.tmpFilePath = this.getConfig().dropbox.tmpFolder + path.replace(/\//g, '-');
    this.stream = fs.createWriteStream(this.tmpFilePath);

    this.stream.on('finish', function () {
        self.emit('stream-finish', self.tmpFilePath);
        self.onStreamFinish();
    });
    return this.stream;
};

/**
 * setter for callback
 */
ParserAbstract.prototype.setCb = function(_cb){
    if ('function' == typeof _cb) {
        this.cb = _cb;
    }
};

/**
 * remove temporary file
 */
ParserAbstract.prototype.onReay = function(content){
    var self = this;
    fs.exists(self.tmpFilePath, function(exists){
        if (exists) {
            fs.unlinkSync(self.tmpFilePath);
        }
    });
    this.cb(content);
};

/**
 * declaration callback
 */
ParserAbstract.prototype.cb = function(){};

/**
 * declaration onStreamFinish
 */
ParserAbstract.prototype.onStreamFinish = function(){};

module.exports = ParserAbstract;

