/**
 * Class: ParserDoc
 * Extends: ParserAbstract
 *
 * @method onStreamFinish
 */

var unzip = require('extract-zip');

var fs = require('fs');
var md5 = require('md5');
var ParserAbstract = require('./Abstract');

ParserDoc.prototype = new ParserAbstract();

function ParserDoc(_cb)
{
    var self = this;
    this.setCb(_cb);

    /**
     * Processing data from Stream
     */
    this.onStreamFinish = () => {
        var distDir = this.getConfig().dropbox.tmpFolder+'dist-'+md5(self.tmpFilePath)
        unzip(self.tmpFilePath, {dir: distDir}, function(err) {
            if (err) {
                return  console.error('Docx Parse Error:', err);
            }

            fs.readFile(distDir+'/word/document.xml', 'utf8', function (err,data) {
                if (err) {
                    return console.log(err);
                }

                var plain_text = data.replace(/(<w:p )[\s\S]*?>/g,"\n<w:p").replace(/(<([^>]+)>)/ig, "");
                self.onReay(plain_text);
            });
        });
    };
}

module.exports = ParserDoc;

