
/**
 * Class: ParserDoc
 * Extends: ParserAbstract
 *
 * @method onStreamFinish
 */
var WordExtractor = require("word-extractor");
var ParserAbstract = require('./Abstract');
ParserDoc.prototype = new ParserAbstract();

function ParserDoc(_cb)
{
    var self = this;
    this.setCb(_cb);

    /**
     * Processing data from Stream
     */
    this.onStreamFinish = () => {
        var extractor = new WordExtractor();
        var extracted = extractor.extract(self.tmpFilePath);
        extracted.then(function(doc) {
            var body = doc.getBody();
            self.onReay(body);
        }, function(err){
            console.error('Doc Parse Error:', err);
            self.onReay('');
        });
    };
}

module.exports = ParserDoc;

