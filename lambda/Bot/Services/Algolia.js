/**
 * Class: Algolia
 * Extends: Abstract
 * Administration data in the Algolia
 *
 * @param index
 *
 * @method getSizeOfContent
 * @method rm
 * @method append
 */

var Abstract = require('../Abstract');
var algoliasearch = require('algoliasearch');
var md5 = require('md5');
var sizeof = require('object-sizeof');

Algolia.prototype = new Abstract();
Algolia.prototype.index = false;

function Algolia() {
    var self = this;
    var config = this.getConfig().algolia;

    var client = algoliasearch(config.appId, config.apiKey, { timeout: 30000} );
    var index = client.initIndex(config.index);

    index.setSettings({
            hitsPerPage: 20,
            attributesToSnippet: [ 'body:10' ],
            attributesForFaceting: [ 'body', 'path', 'url', 'dbxId' ],
            searchableAttributes: [ 'body', 'path', 'dbxId' ],
            attributesToRetrieve: [ 'dbxId' ],
            distinct: true,
            unretrievableAttributes: [ 'dbxId' ],
            attributeForDistinct: 'dbxId',
            highlightPreTag: '<em>',
            highlightPostTag: '</em>'
        },
        function searchDone(err, content) {
            if (err) throw err;
            console.log("Algolia index Settings Updated");
        }
    );
    this.index = index;
}

/**
 * definition size of the data
 */
Algolia.prototype.getSizeOfContent = function() {
    return this.getConfig().algolia.isFreeAccounts ? 10000 : 20000;
};

/**
 * method for remove file contents from Algolia
 */
Algolia.prototype.rm = function(dbxId, _cb) {
    var self = this;
    var rmObjectIDs = [];
    _cb = _cb  ? _cb : ()=>{};
    var runRm = function(){
        if (rmObjectIDs.length) {
            self.index.deleteObjects(rmObjectIDs, function(err, content) {
                if (err) throw err;
                _cb()
            });
        } else {
            _cb()
        }
    };

    var search = function(page) {
        self.index.search({
            query: dbxId,
            distinct: false,
            page: page || 0,
            restrictSearchableAttributes: ['dbxId'],
            attributesToRetrieve: ['dbxId', 'objectID']
        }, function(err, content) {
            if (err) throw err;

            for (var i in content.hits) {
                if (dbxId == content.hits[i].dbxId) {
                    rmObjectIDs.push(content.hits[i].objectID);
                }
            }

            if (!content.hits.length || content.page == content.nbPages) {
                runRm()
            } else {
                search(++page)
            }
        });
    };
    search(0);
};

/**
 * method for append file contents to Algolia
 */
Algolia.prototype.append = function(contents, _cb) {
    var self = this;
    self.rm(contents.dbxId, function() {
        var objects = [];
        var objectID = md5(contents.path);
        var bodyArr = contents.body.replace(/\s\s+/g, ' ').split(' ');
        var j = 0;
        var i = 0;
        var isNextWord = true;

        do {
            var objectsContent = {
                objectID:objectID + '-' + j,
                dbxId:contents.id,
                url:contents.url,
                path:contents.path,
                body:''
            };

            do {
                objectsContent.body +=bodyArr[i] +' ';
                i++;
                isNextWord = 'undefined' != typeof bodyArr[i];
            } while( sizeof(objectsContent) < self.getSizeOfContent() && isNextWord);

            objects.push(objectsContent);
            j++;
        } while( isNextWord );

        self.index.saveObjects(objects, function(err, content) {
            if (err) {
                console.error('Algolia add Object Error:', err);
            }
            console.log('Objects Updated');
            if (_cb) {
                _cb();
            }
        });
    });
};


module.exports = Algolia;



