
/**
 * Class: Queue
 * Extends: Abstract
 * Administration of file Queue
 *
 * @method run
 * @method add
 */

var Abstract = require('../Abstract');
var async = require('async');
Queue.prototype = new Abstract();

function Queue() {
    var fileQueue = {};
    var self = this;

    var run = function(){
        var fileQueueArr = Object.keys(fileQueue);
        if (!fileQueueArr.length) {
            setTimeout(run, 1000);
            return null;
        }

        async.eachOfLimit(fileQueueArr, self.getConfig().dropbox.queueProcessorNumberOfAtOnce, function(path, iteration, asyncCb) {
            var parser = false;
            var parserFilesDownload = false;
            var queueVal = fileQueue[path];

            var finish = ()=> {
                self.services.cache.set(queueVal.id, queueVal.content_hash, function(){
                    if (queueVal._cb){
                        queueVal._cb();
                    }
                    asyncCb();
                });
            };

            var save = (body)=> {
                parser = false; parserFilesDownload = false;

                if ('undefined' != typeof queueVal) {
                    delete fileQueue[path];
                    parser = false;
                }

                if (body) {
                    queueVal.body = body;
                    self.emit('contents', queueVal);
                    new self.dropboxes.SharingGetFileMetadata(queueVal).prepareUrl().then((cntWUrl)=>{
                        self.services.algolia.append(cntWUrl, function() {
                            finish();
                        });
                    });
                } else {
                    finish();
                }
            };

            switch (path.split('.').pop().toLowerCase()) {
                case 'docx':
                    parser = new self.parsers.docx(save);
                    break;
                case 'doc':
                    parser = new self.parsers.doc(save);
                    break;
                case 'pdf':
                    parser = new self.parsers.pdf(save);
                    break;
            }

            if (parser) {
                parserFilesDownload = new self.dropboxes.FilesDownload(fileQueue[path]).initFileStream( parser.getStream(path));
            } else {
                save();
            }

        }, function()
        {
            run();
            self.emit('queue-finished');
            //console.log('DONE!');
        });
    };

    /**
     * add file to the queue
     * @return this
     */
    this.add = function(fileData, cb) {
        if (cb) {
            fileData._cb = cb;
        }
        fileQueue[fileData.path] = fileData;
        return self;
    };

    /**
     * start processing queue
     * @return this
     */
    this.run = function(){
        run();
        return self;
    };
}

module.exports = Queue;



