
/**
 * Class: CacheFile
 * Extends: Abstract
 * Caching data using files
 *
 * @method set
 * @method check
 * @method startWatching
 * @method endWatching
 */

var Abstract = require('../../Abstract');
var fs = require('fs');
var fx = require('mkdir-recursive');
var recursiveReaddir = require('recursive-readdir');
var md5 = require('md5');
var async = require('async');

CacheFile.prototype = new Abstract();

function CacheFile() {
    var self = this;
    var _watching = {};
    var _pathDir = './tmp/cache/';
    var _encoding = 'utf8';


    var getPath = function(key, isIncludeFile) {
        var hesh = md5(key).toLowerCase();
        return   _pathDir + hesh.split('').slice(0, 7).join('/') + '/' + (isIncludeFile ?  key : '' );
    };

    var getFileContents = function(key) {

        return new Promise(function(resolve, reject) {
            var filePath = getPath(key, true);
            fs.exists(filePath, function(existsRes) {
                if (!existsRes) {
                    return resolve(false);
                }

                var cacheText = '';
                try {
                    cacheText =fs.readFileSync(filePath, _encoding);
                }
                catch(e) {
                    return resolve(false);
                }
                return resolve(cacheText);
            });
        });
    };

    var save = function(key, value) {
        var filePath = getPath(key, true);
        var tmpFilePath = filePath+'.tmp';
        fx.mkdir(getPath(key), function(err) {
            fs.writeFile(tmpFilePath, value, _encoding, function(){
                fs.rename(tmpFilePath, filePath, function (err) {
                    if (err) {
                        console.error('Cache Saving Error:', err);
                        return;
                    }
                });
            });
        });
    };

    /*
    this.get = function(key) {
        return getFileContents(key);
    };
    */

    /**
     * functionality for add data to the cache
     * @param string key
     * @param string value
     * @return this
     */
    this.set = function(key, value){
        save(key, value);
        return this;
    };

    /**
     * functionality for check data in the cache
     * @param string key
     * @param string value
     * @param function cb
     */
    this.check = function(key, value, cb) {
        _watching[key] = true;
        getFileContents(key).then((content)=>{
            cb(content && value === content)
        });
    };

    /**
     * start functionality to search for deleted files
     */
    this.startWatching = function(){
        _watching = {};
    };

    /**
     * end functionality to search for deleted files
     */
    this.endWatching = function(){
        var toRm = [];
        var watchingAssign = Object.assign({}, _watching);
        _watching = {};

        recursiveReaddir(_pathDir, ['.gitignore']).then((fileList)=>{
            async.eachOfLimit(fileList, 30, function(filePath, iteration, asyncCb)
            {
                var filePathArr = filePath.split('/');
                var fileName = filePathArr[filePathArr.length-1];
                if ('undefined' == typeof watchingAssign[fileName]) {
                    toRm.push({file:fileName, path:filePath});
                } else {
                    delete watchingAssign[fileName];
                }
                asyncCb();
            }, function()
            {
                for (var i in toRm) {
                    self.emit('dropbox-removed-file', toRm[i].file);
                    fs.unlinkSync(toRm[i].path);
                }
            });
        })
    };
}

module.exports = CacheFile;


