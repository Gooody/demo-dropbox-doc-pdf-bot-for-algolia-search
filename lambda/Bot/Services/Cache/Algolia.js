
/**
 * Class: CacheFile
 * Extends: Abstract
 * Caching data using algolia
 *
 * @method set
 * @method check
 * @method startWatching
 * @method endWatching
 */


var Abstract = require('../../Abstract');
var md5 = require('md5');
var async = require('async');
CacheAlgolia.prototype = new Abstract();

var algoliasearch = require('algoliasearch');

function CacheAlgolia() {
    var self = this;
    var _watching = {};
    var client = algoliasearch(this.getConfig().algolia.appId, this.getConfig().algolia.apiKey, { timeout: 30000} );
    var index = client.initIndex(this.getConfig().algolia.index + '-cache');

    index.setSettings({
            hitsPerPage: 200,
            searchableAttributes: [ 'objectID' ]
        },
        function searchDone(err, content) {
            if (err) throw err;
            console.log("Algolia cache-index Settings Updated");
        }
    );
    this.index = index;

    var getObjectID = function(key){
        return md5(key);
    };

    /**
     * functionality for add data to the cache
     * @param string key
     * @param string value
     * @return this
     */
    this.set = function(key, value, _cb){
        var objects = [{
            objectID:getObjectID(key),
            key:key,
            hash:value
        }];

        self.index.saveObjects(objects, function(err, content) {
            if (err) {
                console.error('Algolia add Object Error:', err);
            }
            console.log('Cache Objects Updated');
            if (_cb) {
                _cb();
            }
        });
        return this;
    };

    /**
     * functionality for check data in the cache
     * @param string key
     * @param string value
     * @param function cb
     */
    this.check = function(key, value, cb) {
        _watching[key] = true;
        self.index.search({
            query: getObjectID(key),
            restrictSearchableAttributes: ['objectID'],
            attributesToRetrieve: ['hash', 'key', 'objectID']
        }, function(err, content) {
            if (err) throw err;

            for (var i in content.hits) {
                if (key == content.hits[i].key) {
                    cb(value === content.hits[i].hash);
                    return true;
                }
            }
            cb(false);
        });
    };

    /**
     * start functionality to search for deleted files
     */
    this.startWatching = function(){
        _watching = {};
    };

    /**
     * end functionality to search for deleted files
     */
    this.endWatching = function(){
        var rmObjectIDs = [];
        var runRm = function(page) {
            if (rmObjectIDs.length) {
                self.index.deleteObjects(rmObjectIDs, function(err, content) {
                    if (err) throw err;
                    self.emit('files-checked-and-cleared');
                });
            } else {
                self.emit('files-checked-and-cleared');
            }
        };

        var searchForRm = function(page) {
            self.index.search({
                query: '',
                page: page || 0,
                attributesToRetrieve: ['key', 'objectID']
            }, function(err, content) {
                if (err) throw err;

                async.eachOfLimit(content.hits, 50, function(entry, iteration, asyncCb) {
                    if ('undefined' == typeof _watching[entry.key]) {
                        self.emit('dropbox-removed-file', entry.key, asyncCb);
                        rmObjectIDs.push(entry.objectID);
                    } else {
                        asyncCb();
                    }
                }, function(){
                    if (!content.hits.length || content.page > content.nbPages) {
                        runRm()
                    } else {
                        searchForRm(++page)
                    }
                });
            });
        };
        searchForRm(0);
    };
}

module.exports = CacheAlgolia;

