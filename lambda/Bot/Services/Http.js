
/**
 * Class: Http
 * Extends: Abstract
 * Processing webhooks
 */

var Abstract = require('../Abstract');
var express = require('express');
Http.prototype = new Abstract();

function Http() {
    var self = this;

    if (!self.getConfig().http.enabled) {
        return self;
    }

    var app = express();
    app.use(express.json());

    app.get('/', function (req, res) {
        res.send('success')
    });

    // route for webhooks
    app.all('/dbx-wh', function (req, res) {
        self.emit('dropbox-changed');
        res.send(req.query.challenge || 'success')
    });

    console.log('listen on', self.getConfig().http.port)
    app.listen(self.getConfig().http.port);
}

module.exports = Http;



