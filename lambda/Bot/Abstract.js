/**
 * Class: Abstract
 *
 * @method getConfig
 * @method setField
 */

var EventEmitter = require('events');
Abstract.prototype = new EventEmitter();
function Abstract() { }

var _config = null;

/**
 * Getter for config data
 * @return _config
 */
Abstract.prototype.getConfig = function(){
    if (!_config) {
        _config = require('./config');
    }
    return _config;
};

/**
 * Setter for Field-value data
 * @param string f
 * @param any v
 * @return this
 */
Abstract.prototype.setField = function(f, v){
    this[f] = v;
    return this;
};

module.exports = Abstract;