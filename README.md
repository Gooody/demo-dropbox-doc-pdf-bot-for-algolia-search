# Installation instruction


###0. System requirements
* AWS Lambda
* AWS Lambda SAM CLI

###1. Installing Lambda NodeJs app
* To clone repository run:
```
     git clone https://github.com/pgahq/dropbox-to-algolia.git
     cd dropbox-to-algolia/
```

* For installing packages run:
```
     cd lambda/
     npm install
```

###2. Get dropbox credentials
* Go to https://www.dropbox.com/developers/apps
* Create a new app
* Mark the params:
  + Choose an API: Dropbox API
  + Choose the type of access, you need: Full Dropbox
  + Name your app: A unique name for your dropbox app
* On the dropbox developers app page - generate access token and save to the config


###3. Get algolia credentials
* Go to Algolia api-keys Dashboard and save a:
* Application ID
* Admin API Key
* Search-Only API Key



###4. Configuring the Lambda NodeJs app
* open for edit  ./template.yaml
* specify Environment Variables (settings and credentials)
```
    AWSLAMBDA_REGION - AWS Lambda Region (for Lambda version Only)
    ALGOLIA_APPID - Algolia Application ID
    ALGOLIA_APIKEY - Algolia admin API Key
    ALGOLIA_INDEX - Algolia index name for dropbox files (will bе created on the algoria service)
    DROPBOX_QUEUE_PROCESSOR_NUMBER_OF_AT_ONCE - number of running file-parsers at the same time
    DROPBOX_TOKEN - dropbox app token
    DROPBOX_PATH - dropbox folder path (if use a root dir - set empty string)
    CACHE_TYPE: - use ‘algolia’ OR ‘file’
        ‘algolia’ - stores the data about the processed files in the algolia index
        ‘file’ - stores the data about the processed files in the files
    TMP_FOLDER - path to the temporary folder
        For Lambda version - use '/tmp/'
        For Node app version - use writable directories (for example ‘./tmp/*’)
```

###6. Deploy Lambda NodeJs app
####Create an Amazon S3 bucket
> aws s3 mb s3://dropbox-doc-pdf-bot-for-algolia-search --region {{region}}

####Create package
> sam package --template-file template.yaml --s3-bucket dropbox-doc-pdf-bot-for-algolia-search --output-template-file packaged.yaml

####Run deploy
> aws cloudformation deploy --template-file packaged.yaml --stack-name dropbox-doc-pdf-bot-for-algolia-search --capabilities CAPABILITY_IAM


###7. Configuring the dropbox webhooks
* Check:
  + If Lambda NodeJs app deploed
  + copy API Gateway Invoke URL
* On the dropbox developers app page - add Webhook URI











